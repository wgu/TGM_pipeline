#!/usr/bin/env perl

##############################################################
## Name: Create_GEX_outputfile.pl                           ##
## Description: Merges the two subset mrna.tsv subset files ##
## and puts the data into the correct format to be used     ##
## as input by MarkerSelection.R                            ##
## Usage: Create_GEX_outputfile.pl <subset1_input_file>     ##
##        <subset2_input_file> <in/output dir> <data_filter>##
## Email: serge.eifes@uni.lu                                ##
##                                                          ##
## This work is licensed under the Creative Commons         ##
## Attribution-NonCommercial-ShareAlike 4.0                 ##
## International License. To view a copy of this license,   ##
## visit http://creativecommons.org/licenses/by-nc-sa/4.0/. ##
##############################################################


###Modules loaded
use strict;
use warnings;



#Argument variables:
my $file_1=$ARGV[0];
my $file_2=$ARGV[1];
my $O_file=$ARGV[2];
#my $data_filter=$ARGV[3]; #Should be either "TRUE" or "FALSE". All values corresponding to -10 will be set to NA
my $data_filter="TRUE"; # test Wei

die "Not allowed argument: $data_filter. Value provided should be 'TRUE' or 'FALSE'" if(($data_filter ne "TRUE") && ($data_filter ne "FALSE"));


my @A_Subset_files;
push(@A_Subset_files, $file_1);
push(@A_Subset_files, $file_2);

my $counter_subset=1;


open(WH, ">$O_file") or die "Cannot open $O_file: $!";


foreach my $file (@A_Subset_files){
	
	#Idx numbers for data columns we want to extract
	my $Patient_ID_idx;
	my $Value_idx;
	my $Probe_ID_idx;
	my $Gene_Symbol_idx;
	
	open(FH, "$file") or die "Cannot open $file: $!";
	
	
	print WH "PATIENT.ID\tVALUE\tPROBE.ID\tGENE_SYMBOL\tSUBSET\n";
	
	while(<FH>){
		
		
		my $Line = $_;
		chomp($Line);
	
	
		my (@A_Line) = split /\t/, $Line;
		
		
		#Retrieving col indices from header for the data we need		
		if($Line=~/PATIENT/){
			for(my $i=0; $i<=$#A_Line; $i++){
				if($A_Line[$i] eq "PATIENT ID"){
					$Patient_ID_idx = $i;
				} elsif($A_Line[$i] eq "LOG2E"){
					$Value_idx = $i;
				} elsif(($A_Line[$i] eq "PROBE") || ($A_Line[$i] eq "ANNOTATIONID")){
					$Probe_ID_idx = $i;
				} elsif($A_Line[$i] eq "GENE SYMBOL"){
					$Gene_Symbol_idx = $i;
				} elsif($A_Line[$i] eq "MIRNA ID"){
					$Probe_ID_idx = $i;
					$Gene_Symbol_idx = $i;
				} 
			}
			next;
		}
		
		my $Patient_ID = $A_Line[$Patient_ID_idx];
		my $Value = $A_Line[$Value_idx];
		my $Probe_ID = $A_Line[$Probe_ID_idx];
		my $Gene_Symbol = $A_Line[$Gene_Symbol_idx];
		
		if($data_filter eq "TRUE"){
			$Value="NA" if$Value==-10;
		}
		
		$Gene_Symbol="NA" if($Gene_Symbol eq "null");
		
		my $Subset;
		$Subset = "S1" if($counter_subset==1);
		$Subset = "S2" if($counter_subset==2);
		
		print WH $Subset . "_" . $Patient_ID . "\t" . $Value 
					 . "\t" . $Probe_ID . "\t" . $Gene_Symbol 
					 . "\t" . $Subset . "\n";
					 
	}
	close FH;
	
	$counter_subset++;
}

close WH;
