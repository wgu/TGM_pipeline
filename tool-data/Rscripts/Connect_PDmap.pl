#!/usr/bin/env perl

##############################################################
## Name: Connect_PDmap.pl				    ##	
## Description: It parses the output file i.e generated by  ##
## differential marker analysis and extracts the gene names ##
## and expression values to PD map			    ##
## Usage: Connect_PDmap.pl <input_file>                     ##
##        <output_file>                                     ##
## Author: venkata.satagopam@uni.lu                         ##
##############################################################

###Modules loaded
use strict;
use warnings;
use LWP::Simple;
use HTTP::Request::Common;
use LWP::UserAgent;	

#Argument variables:
my $file    = $ARGV[0];
my $outfile = $ARGV[1];

my $expression_value="NAME\tVALUE\n";
my @set = ('0' ..'9', 'A' .. 'F');
my $identifier = join '' => map $set[rand @set], 1 .. 16;

## START Input file parser ##
open( FH, "$file" ) or die "Cannot open $file: $!";
my @all = <FH>;
close FH;
shift @all; # it removes header

open( my $ofh,'>', "$outfile" ) or die "Cannot open $outfile: $!";
my %result_hash = ();
while (@all) {
	my $Line = shift @all;
	chomp($Line);
	
	my ($gene_symbol, $probe_id, $log_fc, $t, $p_value, $adj_p_value, $b_value) = split /\t/, $Line;
	my $normalized_log_fc = "";
	$result_hash{$gene_symbol} = $log_fc;			
} # while (<FH>) {

my @log_fc_values = ();

for my $gene_symbol (keys %result_hash) {
	push @log_fc_values, $result_hash{$gene_symbol};
}

@log_fc_values = sort {$a <=> $b} @log_fc_values;
print "log_fc_values : ", join("\t", @log_fc_values), "\n";

my $array_length = scalar(@log_fc_values);
my $min_log_fc_value = $log_fc_values[0];
my $max_log_fc_value = $log_fc_values[$array_length -1];

print "min_log_fc_value : $min_log_fc_value, max_log_fc_value : $max_log_fc_value\n";


my $expression_value="NAME\tVALUE\n";
for my $gene_symbol (keys %result_hash) {
        my $log_fc_value = $result_hash{$gene_symbol};
	if ($log_fc_value > 0) {
		$log_fc_value = $log_fc_value / $max_log_fc_value;
	}
	elsif ($log_fc_value < 0) {
		$log_fc_value = $log_fc_value / abs($min_log_fc_value);
	}
	$expression_value.="$gene_symbol\t$log_fc_value\n";
} # for my $gene_symbol (keys %result_hash) {

my @set = ('0' ..'9', 'A' .. 'F');
my $identifier = join '' => map $set[rand @set], 1 .. 16;
	
my $pdmap_servelet = "http://pg-sandbox.uni.lu:8080/MapViewer/galaxy.xhtml";
my $pdmap_ua = LWP::UserAgent->new();
my $pdmap_req = POST $pdmap_servelet,
	Content => [
		identifier =>  $identifier,
		expression_value => $expression_value,
        ];
my $pdmap_response = $pdmap_ua->request($pdmap_req);
if ($pdmap_response->is_success()) {
	my $pdmaped_content = $pdmap_response->content;
#print "pdmaped_content : $pdmaped_content\n";
	print $ofh "<html>";
		print $ofh "<head></head>";
		print $ofh "<body>";
#print $ofh "<br><h1>pdmaped_content : $pdmaped_content</h1>";
			print $ofh "<br><h3>Overlay of biomarkers on PD map</h3>";
			print $ofh "<h3> Click the <a href=\"http://pg-sandbox.uni.lu:8080/MapViewer/?id=pdmap&layout=$identifier\">PD map</a> link</h3>";
		print $ofh "</body>";
	print $ofh "</html>";
} else {
	print $pdmap_response->as_string;
	print $ofh "<html>";
		print $ofh "<head></head>";
		print $ofh "<body>";
#print $ofh "<br><h1>$pdmap_response->as_string</h1>";
			print $ofh "<br><h3>Overlay of biomarkers on Parkinson's Disease Map</h3>";
			print $ofh "<h3> Click --->  <a href=\"http://pg-sandbox.uni.lu:8080/MapViewer/?id=pdmap&layout=Galaxy\">PD Map</a></h3>";
		print $ofh "</body>";
	print $ofh "</html>";
}	
close $ofh;
## END Input file parser ##

