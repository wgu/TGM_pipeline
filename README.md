## Description of folders and files

### tool_conf.xml
Configuration of what tools to be activated in Galaxy

### tool-data
Folder to store the tool scripts (*.R *.perl... etc)

### tools
XML definitions of each tool